from django.test import TestCase, Client
from django.urls import resolve
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class TestStatusPage(TestCase) :
    def test_Apakah_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)
    
    def test_func(self):
        found = resolve('/') 
        self.assertEqual(found.func, views.home)
    
    def test_apakah_ada_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_konten_apakah_terdapat_nama(self):
         c = Client()
         response = c.get('')
         content = response.content.decode('utf8')
         self.assertIn ("Habel's 7th Story", content)

    def test_konten_apakah_terdapat_daftar_aktivitas(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Aktivitas Saat Ini", content)
    
    def test_konten_apakah_terdapat_daftar_pengalaman(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Pengalaman Organisasi / Kepanitiaan", content)

    def test_konten_apakah_terdapat_daftar_prestasi(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Prestasi", content)

    def test_apakah_terdapat_button(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("button", content)

    def test_apakah_terdapat_accordion(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("accordion", content)
    
class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_theme_mode(self):
        self.browser.get('http://127.0.0.1:8000/')

        # find the element
        acc_1 = self.browser.find_element_by_id('ui-id-1')
        acc_2 = self.browser.find_element_by_id('ui-id-3')
        acc_3 = self.browser.find_element_by_id('ui-id-5')
    
        # test
        time.sleep(1)
      
        time.sleep(1)
        acc_2.click()
        time.sleep(1)
        acc_3.click()
        time.sleep(1)
    
        time.sleep(1)
        acc_1.click()
        time.sleep(1)

        
        time.sleep(5)
        e = self.browser.find_element_by_css_selector(".btn-dark")
        time.sleep(2)
        e.click()
        time.sleep(2)

        time.sleep(5)
        e = self.browser.find_element_by_css_selector(".btn-light")
        time.sleep(2)
        e.click()
        time.sleep(2)

