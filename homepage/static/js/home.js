$(document).ready(function(){
    $(".btn-light").hide(); 
    $("#accordion").accordion();
    
    $('.btn-dark').click(function(){
            $(".btn-dark").hide(); 
            
            $(".btn-light").show();
            
            $("body").animate({
                "background-color" : "black"
            });
            
            $("h1").animate({
                "color" : "white"
            });

            $("h2").animate({
                "color" : "white"
            });

            $("h3").animate({
                "color" : "black"
            });

            $('.kepala').animate({
                "background-color" : "white"
            });

            $(".isi").animate({
                "background-color":"black"
            });

            $("p").animate({
                "color" : "white"
            });

            $("li").animate({
                "color" : "white"
            });

    });

    $('.btn-light').click(function(){
            $(".btn-light").hide();
            $(".btn-dark").show();
            
            
            $("body").animate({
                "background-color" : "white"
            });

            
            $("h1").animate({
                "color" : "black"
            });

            $("h2").animate({
                "color" : "black"
            });

            $("h3").animate({
                "color" : "white"
            });

            $(".isi").animate({
                "background-color":"white"
            });

            $('.kepala').animate({
                "background-color" : "black"
            });

            $("p").animate({
                "color" : "black"
            });

            $("li").animate({
                "color" : "black"
            });
            

    });

   });